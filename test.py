from retro_star.api import RSPlanner

planner = RSPlanner(
    gpu=-1,
    use_value_fn=True,
    iterations=100,
    expansion_topk=50,
    viz=True
)

result = planner.plan('O=C(O)c2cc(O)c(Oc1ccc(Cl)cc1)c(O)c2')
print(result)